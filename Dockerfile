FROM python:3.9.1
RUN  \
     apt-get -y update && \
     apt-get install -y zip && \
     rm -rf /var/lib/apt/lists/* && \
     mkdir /app
COPY delfi-gtfs-augmenter.py /app
RUN chmod a+x /app/delfi-gtfs-augmenter.py
WORKDIR /app
CMD ["delfi-gtfs-augmenter.py"]