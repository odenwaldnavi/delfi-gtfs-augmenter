#!/usr/bin/python3

import csv


#
# get ids of all stations in stops.csv
#

#"stop_id","stop_code","stop_name","stop_desc","stop_lat","stop_lon","location_type","parent_station","wheelchair_boarding","platform_code","level_id"
#"de:06413:8041:2:2","","Offenbach (Main)-Rumpenheim Kurhessenplatz","NRumpenheim","50.129671000000","8.797541000000",0,,0,"","2"

# location_type 1 is the station
# location_type 0 is the platform/stop

class GTFSDialect(csv.Dialect):
        delimiter      = ','
        doublequote    = True
        lineterminator = '\r\n'
        quotechar      = '"'
        quoting        = csv.QUOTE_NONNUMERIC

stations = {}

with open('stops.txt', encoding='utf-8') as f:
    reader = csv.DictReader(f, dialect=GTFSDialect)

    for row in reader:
        if (row['location_type'] == 1):
            stations[row['stop_id']] = True


#
# get stop ids with their station if from zhv.csv
#

#"SeqNo";"Type";"DHID";"Parent";"Name";"Latitude";"Longitude";"MunicipalityCode";"Municipality";"DistrictCode";"District";"Condition";"State";"Description";"Authority";"DelfiName";"TariffDHID";"TariffName"
#"424243";"Q";"de:06437:16100:1:1";"de:06437:16100:1";"NFürth";"49,682039";"8,806568";"00000000";"-";"-";"-";"Served";"InOrder";"";"rms";"-";"-";"-"
#"424244";"Q";"de:06437:16100:2:2";"de:06437:16100:2";"VFürth";"49,68203";"8,806596";"00000000";"-";"-";"-";"Served";"InOrder";"";"rms";"-";"-";"-"

# "S" is parent of "A" is parent of "Q"

class ZHVDialect(csv.Dialect):
        delimiter      = ';'
        doublequote    = True
        lineterminator = '\r\n'
        quotechar      = '"'
        quoting        = csv.QUOTE_MINIMAL

area_to_station = {}
# additional stations not yet in GTFS stops
zhv_stations = {}

with open('zHV_aktuell_csv.csv', encoding='utf-8-sig') as f:
    reader = csv.DictReader(f, dialect=ZHVDialect)

    for row in reader:
        if (row['Type'] == 'S' and row['DHID'] not in stations):
            zhv_stations[row['DHID']] = True
        if (row['Parent'] and row['Type'] == 'A'):
            area_to_station[row['DHID']] = row['Parent']

quay_to_station = {}

with open('zHV_aktuell_csv.csv', encoding='utf-8-sig') as f:
    reader = csv.DictReader(f, dialect=ZHVDialect)

    for row in reader:
        if (row['Parent'] and row['Type'] == 'Q' and row['Parent'] in area_to_station):
            quay_to_station[row['DHID']] = area_to_station[row['Parent']]


#
# augment stops.csv filling in parent_station for stops
#

#"stop_id","stop_code","stop_name","stop_desc","stop_lat","stop_lon","location_type","parent_station","wheelchair_boarding","platform_code","level_id"

fieldnames = ['stop_id','stop_code','stop_name','stop_desc','stop_lat','stop_lon','location_type','parent_station','wheelchair_boarding','platform_code','level_id']

with open('stops.txt', encoding='utf-8') as f, open('stops-with-parents.txt', mode='w', newline='', encoding='utf-8') as t:
    reader = csv.DictReader(f, dialect=GTFSDialect)
    writer = csv.DictWriter(t, dialect=GTFSDialect, fieldnames=fieldnames)

    writer.writeheader()

    for row in reader:
        row['location_type'] = int(row['location_type'])
        row['wheelchair_boarding'] = int(row['wheelchair_boarding'])
        # add reference to parent staion to stops
        if (row['location_type'] == 0 and not row['parent_station'] and row['stop_id'] in quay_to_station):
            row['parent_station'] = quay_to_station[row['stop_id']]
        # change location type of stations to station if its a stop, regardsless of wrongly being used as stop!
        if (row['location_type'] == 0 and not row['parent_station'] and row['stop_id'] in zhv_stations):
            row['location_type'] = 1
            print ('changing location_type of stop ' + row['stop_id'] + ' to station')
# to KISS we can't write empty fields, so don't try
#        if row['parent_station'] == "":
#            row['parent_station'] =  None
        writer.writerow(row)

# and now only remove "" from the parent_station, but not the platform_code, to reduce the delta
